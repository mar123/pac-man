class Person :
    has_eyes= True
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self) :
        print ('Hello, I am', self.name)

Lidia = Person('Lidia', 16)
Marin = Person('Marin', 12)
Mario = Person('Mario', 11)

people = [Lidia, Marin, Mario]

for p in people :
    p.say_hello()