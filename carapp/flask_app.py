from flask import Flask, jsonify, request
from pony.orm import Database, select, Required, Optional, db_session

app = Flask(__name__)

db = Database()

class Car(db.Entity):
    make = Required(str)
    model = Required(str)
    year = Required(int)
    km = Optional(int)

db.bind(provider = 'sqlite', filename = 'cardb', create_db = True)
db.generate_mapping(create_tables = True)

# create your view and endpoint to view cars
# create another view and endpoint to create cars

@db_session
@app.route('/')
def index():
    return 'hello'

@db_session
@app.route('/addcar', methods = ['GET', 'POST'])
def addcar():
    return 'goodbye'
