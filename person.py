class Person :
    def __init__(self, name,special_message) :
        self.name = name
        self.special_message = special_message

    def get_in_car(self, car):
        car.pasagers.append(self)
        print(f'{self.name} got inside {car.name}')

    def shout(self):
        print(self.special_message.upper())

