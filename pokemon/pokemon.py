class Pokemon:

    def __init__(self, name, health, ptype):
        self.name = name
        self.health = health
        self.ptype = ptype
        self.moves = []