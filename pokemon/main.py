from pokemon import Pokemon
from moves import Move
from players import Player
from potions import Potion

p1 = Player('Johhny')
p2 = Player('Openent')

firepika = Pokemon('Pikachu', 100, 'fire')
vaporeon = Pokemon('Vaporeon', 80, 'water')
bulbasur = Pokemon('Bulbasur', 90, 'grass')
firetwo = Pokemon('Firetwo', 1000, 'fire')

tackle = Move('Tackle', 5, 'Normal', 30)
fireblast = Move('Fire Blast', 100000, 'Fire',5)
bite = Move('Bite', 25, 'Normal', 20)
watergun = Move('Watergun', 35, 'Water', 15)
solarbeam = Move('Solar Beam', 45, 'Fire', 10)
scratch = Move('Scratch', 10, 'Normal', 25)
bodyslam = Move('Body Slam', 15, 'Normal', 35)

bulbasaur.moves.extend([bite, solarbeam, scratch])

p1.pokemons.append(bulbasaur)

#import os

class Battle:
    p1poke = 'Bulbasaur'
    p2poke = 'Vaporeon'


def main_menu():
    choices = ['Moves': move_menu, 'Pokemon': pokemon_menu, 'Potions': potions_menu]
    while True:
#        os.system('clear')
        print('Main Menu')
        print(f'"{Battle.p1poke}  vs  {Battle.p2poke}")
        for i in choices:
            print (i)

        choice = imput('Please make a selection')
        if choice in choices:
            choices[choice]()

def poke_menu():
    player_poke = ['Chizard', 'Pikachu', 'Warturtle']

    while True:
        os.system('clear')
        print('These are your pokemon')
        for pokemon in player_poke:
            print(pokemon)
        print('Cancel')



