def add(x,y,z):
    # this function should return the sum of any three numbers
    return x + y + z

def subtract(x, y):
    # this function should subtract two numbers and return the value
    return x - y

def sum_of_list(some_list):
    # this function should return the sum of all numbers in a given list

    count = 0
    for i in some_list:
        count=count+i

    return count

def shout(some_phrase):
    return some_phrase.upper()
    # this function should convert any string you pass into all uppercase
    # and print it

    print(some_phrase)

def combine_three_lists(x, y, z):
    return list(set(x+z+y))
    # this function should combine three lists and remove any
    # repeated numbers in the final list. return the final list

print(combine_three_lists([2, 3], [6, 8], [2, 6]))